# ======================================================================================================================
# === DATABASE =========================================================================================================
# ======================================================================================================================
class DataType:
    TEXT = "text"
    INT = "int"


# ======================================================================================================================
# === MISCELLANEOUS=====================================================================================================
# ======================================================================================================================
WAITING_PERIOD = 120
HEADER = {'User-Agent': 'Chrome/50.0.2661.102'}